<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>{{ $order->order_code }}</title>
        <style>
            table {
                width: 100%;
            }

            .items,
            .tr-items,
            .td-items {
                padding: 10px;
                border: 1px solid black;
                border-collapse: collapse;
            }

            .status {
                float: right;
            }

            ul {
                list-style-type: none;
            }

            .tr-status>td {
                padding: 10px;
            }
        </style>
    </head>

    <body>
        <h3 style="background-color: black; color: white; padding:10px;">INVOICE#{{ $order->order_code }}</h3>
        <small><i>Tanggal invoice dicetak: {{ date('d-m-Y', time()) }}</i></small>
        <table>
            <tr>
                <td>
                    <div style="padding: 40px;">
                        <small><i>Dari:</i></small> <br>
                        {{ $setting->web_name }} <br>
                        {{ $setting->phone_number }} <br>
                        {{ $setting->address }}
                    </div>
                </td>
                <td>
                    <div style="padding: 40px;">
                        <small><i>Untuk:</i></small> <br>
                        {{ $order->owner_name }} <br>
                        {{ $order->owner_contact }} <br>
                    </div>
                </td>
            </tr>
        </table>

        <table class="items">
            <tr class="tr-items">
                <td class="td-items"><b>Nama service</b></td>
                <td class="td-items"><b>QTY</b></td>
                <td class="td-items"><b>Harga perunit</b></td>
            </tr>
            <tr class="tr-items">
                <td class="td-items">{{ $order->service->name }}</td>
                <td class="td-items">{{ $order->payment->qty }}</td>
                <td class="td-items">Rp. {{ number_format($order->service->price, 2, ',', '.') }}</td>
            </tr>
        </table>

        <div class="status">
            <table style="width: 30%;">
                <tr class="tr-status">
                    <td style="background-color: black; color: white;">Kode order</td>
                    <td><i>{{ $order->order_code }}</i></td>
                </tr>
                <tr class="tr-status">
                    <td style="background-color: black; color: white;">Status pembayaran</td>
                    <td><i>{{ $order->payment->status }}</i></td>
                </tr>
                <tr class="tr-status">
                    <td style="background-color: black; color: white;">Total pembayaran</td>
                    <td>Rp. {{ number_format($order->payment->total, 2, ',', '.') }}</td>
                </tr>
            </table>
        </div>

    </body>

</html>
