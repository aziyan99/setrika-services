<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class InstallCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'install:app';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Start installation the app';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->info('Migrating database');
        Artisan::call("migrate:fresh");
        $this->info('Seeding database');
        Artisan::call("db:seed --class=DefaultSeeder");
        $this->info('Linkin Storage');
        Artisan::call("storage:link");
        $this->info('Some optimize');
        Artisan::call("config:cache");
        Artisan::call("route:cache");
        Artisan::call("view:cache");
        $this->info('Done :)');
    }
}
