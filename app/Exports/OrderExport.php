<?php

namespace App\Exports;

use App\Models\Order;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class OrderExport implements FromView
{
    public function view(): View
    {
        return view('backend.order.report.report', [
            'orders' => Order::with('payment', 'service')->get()
        ]);
    }
}
