<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Models\Setting;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.index', [
            'setting' => Setting::first(),
            'services' => Service::where('status', 'active')->get()
        ]);
    }
}
