<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'contact' => 'required',
            'msg' => 'required'
        ]);
        Contact::create($request->only('name', 'contact', 'msg'));
        return back()->with(['success' => 'Terima kasih telah menghubungi kami!']);
    }
}
