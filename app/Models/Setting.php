<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $fillable = [
        'web_name', 'address', 'phone_number', 'facebook', 'instagram', 'header_msg', 'contact_msg', 'footer_msg'
    ];
}
