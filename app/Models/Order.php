<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $fillable = [
        'service_id', 'owner_name', 'owner_contact', 'status', 'desc', 'order_code'
    ];

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function payment()
    {
        return $this->hasOne('App\Models\Payment');
    }
}
