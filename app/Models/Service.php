<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    public $timestamps = true;

    protected $fillable = [
        'name', 'price', 'status', 'desc'
    ];

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }
}
