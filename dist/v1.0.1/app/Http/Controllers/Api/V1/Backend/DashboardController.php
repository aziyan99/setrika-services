<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Service;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function getTotalData()
    {
        $totalUsers = \DB::table('users')->count();
        $totalOrders = \DB::table('orders')->count();
        $totalServices = \DB::table('services')->count();
        $estimationMoney = \DB::table('payments')->select('total')->sum('total');
        $messages = Contact::all();
        $rawServices = Service::with('orders')->get();

        $services = [];
        for ($i = 0; $i < count($rawServices); $i++) {
            $data = [
                'id' => $rawServices[$i]->id,
                'name' => $rawServices[$i]->name,
                'total' => count($rawServices[$i]->orders)
            ];
            array_push($services, $data);
        }

        return response()->json([
            'total_users' => $totalUsers,
            'total_orders' => $totalOrders,
            'total_services' => $totalServices,
            'estimation_money' => $estimationMoney,
            'services' => $services,
            'messages' => $messages
        ], 200);
    }
}
