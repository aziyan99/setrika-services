<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Exports\OrderExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ReportController extends Controller
{

    /**
     * Sanctum check
     */
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    public function reportAllOrder()
    {
        $fileName = "laporan-" . time() . ".xlsx";
        return Excel::download(new OrderExport, $fileName);
    }
}
