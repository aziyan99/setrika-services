<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Backend\OrderResource;
use App\Models\Order;
use App\Models\Payment;
use App\Models\Service;
use App\Models\Setting;
use Illuminate\Http\Request;
use PDF;

class OrderController extends Controller
{
    /**
     * Auth check
     */
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $count = $request->count ?? 10;
        $services = Service::where('status', 'active')->get();
        if ($request->keywords != null) {
            $orders = new OrderResource(Order::where('owner_name', 'like', '%' . $request->keywords . '%')
                ->orWhere('owner_contact', 'like', '%' . $request->keywords . '%')
                ->orWhere('desc', 'like', '%' . $request->keywords . '%')
                ->orWhere('order_code', 'like', '%' . $request->keywords . '%')
                ->with('payment', 'service')
                ->paginate($count));
            return response()
                ->json([
                    'orders' => $orders,
                    'services' => $services
                ], 200);
        } else {
            $orders = new OrderResource(Order::with('payment', 'service')->latest()->paginate($count));
            return response()
                ->json([
                    'orders' => $orders,
                    'services' => $services
                ], 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'owner_name' => 'required',
            'owner_contact' => 'required',
            'service_id' => 'required',
            'desc' => 'required',
        ]);
        $orderCode = "ORD-" . time();
        $order = Order::create($request->only('owner_name', 'owner_contact', 'service_id', 'desc') + ['order_code' => $orderCode]);
        Payment::create([
            'order_id' => $order->id,
            'qty' => 0,
            'total' => 0
        ]);
        return response()
            ->json(['msg' => 'ok!'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        return response()
            ->json(Order::with('payment')->findOrFail($order->id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        $request->validate([
            'owner_name' => 'required',
            'owner_contact' => 'required',
            'service_id' => 'required',
            'desc' => 'required',
        ]);
        $order->update($request->only('owner_name', 'owner_contact', 'service_id', 'desc'));
        return response()
            ->json(['msg' => 'ok!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        $order->delete();
        return response()
            ->json(['msg' => 'ok!'], 200);
    }

    /**
     * Bulk destroy
     *
     * @param Request $request
     * @return void
     */
    public function bulkDestroy(Request $request)
    {
        $data = $request->id;
        for ($i = 0; $i < count($data); $i++) {
            $order = Order::find($data[$i]);
            $order->delete();
        }
        return response()
            ->json(['msg' => 'ok!'], 200);
    }

    /**
     * Update order status
     *
     * @param Request $request
     * @param [type] $id
     * @return void
     */
    public function updateStatusOrder(Request $request, $id)
    {
        $order = Order::findOrFail($id);
        $order->update([
            'status' => $request->status
        ]);
        return response()
            ->json(['msg' => 'ok!'], 200);
    }

    public function updatePayment(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'qty' => 'required',
            'total' => 'required',
            'status' => 'required'
        ]);
        $payment = Payment::findOrFail($request->id);
        $payment->update($request->only('qty', 'status', 'total'));
        return response()
            ->json(['msg' => 'ok!'], 200);
    }

    public function createInvoice($id)
    {
        $order = Order::with('payment', 'service')->findOrFail($id);
        $setting = Setting::first();

        $pdf = PDF::loadview('backend.order.invoice.invoice', [
            'order' => $order,
            'setting' => $setting
        ])->setPaper('a4', 'landscape');
        return $pdf->stream();
    }
}
