<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function destroy($id)
    {
        Contact::destroy($id);
        return response()->json([], 200);
    }
}
