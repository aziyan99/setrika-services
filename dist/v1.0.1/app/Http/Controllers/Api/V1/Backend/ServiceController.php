<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\Backend\ServiceResource;
use App\Models\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

    /**
     * Auth check
     */
    public function __construct()
    {
        $this->middleware('auth:sanctum');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $count = $request->count ?? 10;
        if ($request->keywords != null) {
            $services = new ServiceResource(Service::where('name', 'like', '%' . $request->keywords . '%')
                ->orWhere('price', 'like', '%' . $request->keywords . '%')
                ->orWhere('status', 'like', '%' . $request->keywords . '%')
                ->paginate($count));
            return response()
                ->json($services, 200);
        } else {
            $services = new ServiceResource(Service::latest()->paginate($count));
            return response()
                ->json($services, 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:services,name',
            'price' => 'required|numeric',
            'desc' => 'required',
            'status' => 'required'
        ]);
        Service::create($request->only('status', 'name', 'price', 'desc'));
        return response()
            ->json(['msg' => 'ok!'], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        return response()
            ->json($service, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $request->validate([
            'name' => 'required|unique:services,name,' . $service->id,
            'price' => 'required|numeric',
            'desc' => 'required',
            'status' => 'required'
        ]);
        $service->update($request->only('status', 'name', 'price', 'desc'));
        return response()
            ->json(['msg' => 'ok!'], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        $service->delete();
        return response()
            ->json(['msg' => 'ok!'], 200);
    }

    public function bulkDestroy(Request $request)
    {
        $data = $request->id;
        for ($i = 0; $i < count($data); $i++) {
            $service = Service::find($data[$i]);
            $service->delete();
        }
        return response()
            ->json(['msg' => 'ok!'], 200);
    }
}
