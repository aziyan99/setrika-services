<?php

namespace App\Http\Controllers\Api\V1\Backend;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function index()
    {
        return response()->json(['setting' => Setting::first()], 200);
    }

    public function update(Request $request)
    {
        $request->validate([
            'web_name' => 'required',
            'phone_number' => 'required',
            'address' => 'required',
            'facebook' => 'required',
            'instagram' => 'required',
            'header_msg' => 'required',
            'contact_msg' => 'required',
            'footer_msg' => 'required',
        ]);
        \DB::table('settings')->truncate();
        Setting::create([
            'web_name' => $request->web_name,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
            'facebook' => $request->facebook,
            'instagram' => $request->instagram,
            'header_msg' => $request->header_msg,
            'contact_msg' => $request->contact_msg,
            'footer_msg' => $request->footer_msg,
        ]);
        return response()->json([], 201);
    }
}
