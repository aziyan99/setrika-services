<table>
    <thead>
        <tr>
            <th>ID</th>
            <th>Kode order</th>
            <th>Nama pemilik</th>
            <th>Kontak pemilik</th>
            <th>Status pesanan</th>
            <th>Layanan yang digunakan</th>
            <th>QTY</th>
            <th>Total pembayaran</th>
            <th>Status pembayaran</th>
            <th>Tanggal pesanan masuk</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($orders as $order)
        <tr>
            <td>{{ $order->id }}</td>
            <td>{{ $order->order_code }}</td>
            <td>{{ $order->owner_name }}</td>
            <td>{{ $order->owner_contact }}</td>
            <td>{{ $order->status }}</td>
            <td>{{ $order->service->name }}</td>
            <td>{{ $order->payment->qty }}</td>
            <td>Rp. {{ number_format($order->payment->total, 2, ',', '.') }}</td>
            <td>{{ $order->payment->status }}</td>
            <td>{{ $order->created_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
