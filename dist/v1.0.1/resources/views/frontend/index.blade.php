<!DOCTYPE html>
<html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
            integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl"
            crossorigin="anonymous" />
        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <style>
            html,
            body {
                height: 100%;
                font-size: 1rem;
            }
            .cloth-img {
                width: 300px;
                height: 300px;
            }
            li>a{
                color: black;
            }

        </style>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.0/font/bootstrap-icons.css">

        <title>{{ $setting->web_name }}</title>
    </head>

    <body>
        <nav id="nav" class="navbar navbar-light bg-light fixed-top border border-top-0 border-left-0 border-right-0">
            <div class="container">
                <a class="navbar-brand" href="{{ route('home') }}">{{ $setting->web_name }}</a>
                <ul class="nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#home">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#service">Service</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
        </nav>
        <div data-bs-spy="scroll" data-bs-target="#nav" data-bs-offset="0" tabindex="0"></div>
        <header class="bg-white p-5" id="home">
            <div class="container p-5">
                <div class="row">
                    <div class="col-md-6">
                        <div class="p-5">
                            <h3>{{ $setting->web_name }}</h3>
                            <p>
                                {!! $setting->header_msg !!}
                            </p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <img class="cloth-img mx-auto d-block"
                            src="https://images.unsplash.com/photo-1523972426550-23b522ad4f9f?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=2551&q=80"
                            alt="cloth">
                    </div>
                </div>
            </div>
        </header>
        <main class="bg-light p-5 border border-left-0 border-right-0" id="service">
            <div class="container p-5">
                <div class="row">
                    @foreach ($services as $service)
                    <div class="col-md-4">
                        <div class="card">
                            <div class="card-body">
                                <h3 class="text-center">{{ $service->name }}</h3>
                                <hr>
                                <p>
                                    {!! $service->desc !!}
                                </p>
                            </div>
                            <div class="card-footer text-center">
                                Rp. {{ number_format($service->price, 2, ',', '.') }}
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </main>
        <div class="bg-white p-5" id="contact">
            <div class="container p-5">
                <div class="row">
                    <div class="col-md-4">
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ $message }}
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                        @endif
                        <form action="{{ route('contact.store') }}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label>Nama</label>
                                <input name="name" type="text" class="form-control @error('name')
                  is-invalid
              @enderror" value="{{ old('name') }}">
                                @error('name')
                                <small class="invalid-feedback" role="alert">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label>Kontak(Email/No.Hp)</label>
                                <input name="contact" type="text" class="form-control @error('contact')
                  is-invalid
              @enderror" value="{{ old('name') }}">
                                @error('contact')
                                <small class="invalid-feedback" role="alert">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label>Pesan</label>
                                <textarea name="msg" cols="30" rows="5" class="form-control @error('msg')
                  is-invalid
              @enderror">{{ old('msg') }}</textarea>
                                @error('msg')
                                <small class="invalid-feedback" role="alert">{{ $message }}</small>
                                @enderror
                            </div>
                            <div class="d-grid gap-2">
                                <button class="btn btn-primary" type="submit">Kirim</button>
                            </div>

                        </form>
                    </div>
                    <div class="col-md-8">
                        <div class="p-5">
                            <h3>{{ $setting->web_name }}</h3>
                            <p>
                                {!! $setting->contact_msg !!}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
        <footer class="bg-light p-5 border border-left-0 border-right-0">
            <div class="container p-5">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{{ $setting->web_name }}</h4>
                        <small>
                            {!! $setting->footer_msg !!}
                        </small>
                    </div>
                    <div class="col-md-2">
                        <h4>Menu</h4>
                        <ul>
                            <li>Home</li>
                            <li>Service</li>
                            <li>Contact</li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <p>{{ $setting->address }}</p>
                        <p>{{ $setting->phone_number }}</p>
                        <a target="_blank" href="{{ $setting->facebook }}" class="me-2"><i
                                class="fs-5 bi bi-facebook"></i></a>
                        <a target="_blank" href="{{ $setting->instagram }}"><i class="fs-5 bi bi-instagram"></i></a>
                    </div>
                </div>
            </div>
        </footer>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
        </script>
    </body>

</html>
