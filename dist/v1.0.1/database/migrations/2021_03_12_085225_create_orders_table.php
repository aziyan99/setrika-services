<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->foreignId('service_id')
                ->references('id')
                ->on('services')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->string('order_code');
            $table->string('owner_name');
            $table->string('owner_contact');
            $table->enum('status', ['ongoing', 'process', 'done'])->default('ongoing');
            $table->longText('desc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
